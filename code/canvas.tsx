// WARNING: this file is auto generated, any changes will be lost
import { createDesignComponent, CanvasStore } from "framer"
const canvas = CanvasStore.shared(); // CANVAS_DATA;
export const Bottom_Buttons = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_l_bm5MjVu", {}, 330,50);
export const Button = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_V95eF_kml", {}, 336,50);
export const Logo = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_AuHvUJS9g", {}, 42,94);
export const Modal = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_hxoE69v0o", {}, 375,812);

export const colors = Object.freeze({
    /** #636363 */
    "TEXT": "var(--token-d227f892-bd0d-41ab-b56f-232f5af4e6b8, rgb(99, 99, 99))",
})
