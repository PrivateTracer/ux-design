import { Data, Override } from "framer"

const data = Data({
    rotate: 0,
    rotateY: 0,
    toggle: true,
})

export function Hover(): Override {
    return {
        whileTap: { scale: 0.9 },
    }
}

export function Draggable(): Override {
    return {
        drag: true,
    }
}

export function Rotate(): Override {
    return {
        animate: { rotate: 180 },
        transition: { duration: 1 },
        // onTap() {
        //     data.rotate = data.rotate + 90
        // },
    }
}

export function FlipInput(): Override {
    return {
        onTap() {
            const toggle = data.toggle
            data.rotateY = toggle ? 180 : 0
            data.toggle = !toggle
        },
    }
}

export function FlipOutput(): Override {
    return {
        animate: { rotateY: data.rotateY },
    }
}

export function Move1(): Override {
    return {
        animate: { x: 100 },
        transition: { duration: 20 },
    }
}

export function Move2(): Override {
    return {
        animate: { x: -100 },
        transition: { duration: 30 },
    }
}

export function Move3(): Override {
    return {
        animate: { x: 100 },
        transition: { duration: 40 },
    }
}

export function Move4(): Override {
    return {
        animate: { x: 100 },
        transition: { duration: 50 },
    }
}
