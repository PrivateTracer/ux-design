# UX Design

This is the UX design for the Private Tracer app. It is built using Framer X.

<img src="https://cdn.discordapp.com/attachments/695937924105240627/699988528653926560/1.png" alt="Screen1" width="200"/><img src="https://cdn.discordapp.com/attachments/695937924105240627/699988532726333500/2.png" alt="Screen2" width="200"/><img src="https://cdn.discordapp.com/attachments/695937924105240627/699988534357917696/3.png" alt="Screen3" width="200"/>

## Installation
Clone the repo

In your terminal, cd into the directory you just created

Type `npm install` to install all dependencies

Open the .framerfx folder with Framer X

To look at the code, just open up the project in your favorite code editor.

## Preview
You can preview this design directly in the browser here:
[Demo v0.5.9](https://private-tracer-v0-5-9.now.sh/)


You can also preview in the Framer [iOS app](https://itunes.apple.com/app/id1124920547) or [Android app](https://play.google.com/store/apps/details?id=com.framerjs.android).

1. Download the Framer Preview app

<img src="https://cdn.discordapp.com/attachments/695937924105240627/705525411492134932/code.png" alt="QR Code" width="200"/>

2. On iOS: Scan the QR code within the app. On Android: Copy [this link](https://private-tracer-v0-5-9.now.sh/) to your device's clipboard. It will now automatically show up under the `Recents` tab in the Framer Preview app.

## Developer Handoff
UI icons are from [FeatherIcons](https://feathericons.com)

App icons are located in `/app icons`

Assets are located in `/assets`

Because every Framer project is ultimately a web project, you can use your browser’s inspector to obtain colors and styles from Framer X projects.

To learn how the web inspector works watch this [Apple Safari Web Inspector](https://developer.apple.com/videos/play/tech-talks/401/) Tutorial.



## Contributing
Please contact [Mesbah Sabur](https://gitlab.com/mesbahsabur)



## License
[MIT](https://choosealicense.com/licenses/mit/)